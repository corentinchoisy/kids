# KIdney Donor toolS

## Description
KIDS is an automated calculator for the FKDMS and RKDMS kidney donor marginality scores for French kidney donors (publication pending). It allows for an easy computation of these scores from user input on a simple GUI.

![Screenshot](https://i.ibb.co/4KSMs3s/screenshotshiny.png)

## Dependencies
You can run this app locally using any functional RShiny installation with the following packages installed:
* `shinydashboard`
* `shinyBS`
* `shinyWidgets`

## Usage (RStudio)

1. Download this repository's content and open either `app_en.R` or `app_fr.R` in RStudio (depending on which language you wish to use)
2. Click on the `Run App` button in the top right corner of the code section

## Support
For support, reach me through this repository's issues, via email (corentin.choisy@univ-nantes.fr) or on Mastodon (@corentinchoisy).

## License
This project is licensed under the GPLv3 General Public License. See the [LICENSE](LICENSE) file for more details.

