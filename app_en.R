#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(shinydashboard)
library(shinyBS)
library(shinyWidgets)

conditionalPanel2 <- function(condition, ..., ns = NS(NULL), inline = FALSE, container = if (inline) span else div) {
  container(`data-display-if` = condition, `data-ns-prefix` = ns(""), ...)
}
conditionalMenuItem <- function(label,tabName,iconName,condition) {
  t <- menuItem(
    label,
    tabName = tabName,
    icon = icon(iconName)
  )
  t$attribs <- list('data-display-if'=condition,'data-ns-prefix'='')
  t
}
# Define UI for application that draws a histogram
countries <- c('Français','English')
flags <- c('https://cdn.rawgit.com/lipis/flag-icon-css/master/flags/4x3/fr.svg','https://cdn.rawgit.com/lipis/flag-icon-css/master/flags/4x3/gb.svg')
ui <- 
  dashboardPage(
    dashboardHeader(title = "KIdney Donor toolS",
                    tags$li(class = "dropdown",div(style="margin-top: 8px; margin-bottom: -8px",actionButton(style='background-color: #d32927; border-color: #d32927;',"eng",HTML(paste(img(src=flags[2],width=20,height=15))),value="Open in english",onclick="window.open('https://corentin-choisy.shinyapps.io/kids_en/','_self')"
                                                                                                             ))),
                    tags$li(class = "dropdown",div(style="margin-top: 8px; margin-bottom: -8px",actionButton(style='background-color: #d32927; border-color: #d32927;',"fra",HTML(paste(img(src=flags[1],width=20,height=15))),value="Ouvrir en Français",onclick="window.open('https://corentin-choisy.shinyapps.io/kids_fr/','_self')"
                    ))),
                    dropdownMenu(type = "notifications",
                                 notificationItem(
                                   text = "Back to software list",
                                   icon = icon("right-from-bracket"),
                                   href = 'https://sphere-inserm.fr/en/valorizations/software',
                                   status = 'danger'),
                                 badgeStatus = NULL,
                                 icon = icon('right-from-bracket'),
                                 headerText = ''
                    ),
                    dropdownMenu(type='messages',
                                 messageItem(from = 'Inserm UMR 1246 SPHERE',
                                             message = 'Visit the website',
                                             icon = icon('globe'),
                                             href = 'https://sphere-inserm.fr'),
                                 messageItem(from = 'DIVAT',
                                             message = 'Visit the website',
                                             icon = icon('square-poll-vertical'),
                                             href = 'https://divat.fr'),
                                 messageItem(from = 'View code on Gitlab',
                                             message = 'View and download',
                                             icon = icon('gitlab'),
                                             href = 'https://gitlab.com/corentinchoisy/kids'),
                                 messageItem(from = 'About',
                                             message = 'Authors & info.',
                                             icon = icon('code'),
                                             href = 'https://corentinchoisy.gitlab.io/rkdms-landing/'),
                                 badgeStatus = NULL,
                                 icon = icon('circle-info'),
                                 headerText = "More information")
    ),
    
    dashboardSidebar(
      collapsed=TRUE,
      sidebarMenu(
        menuItem("FKDMS & RKDMS", tabName = "rkdms", icon = icon("gauge-high"))
      )
    ),
    
    
    
    
    
    
    
    
    
    
    
    
    
    dashboardBody(
      tabItems(tabItem('rkdms',
                       column(width = 5,
                              box(
                                title = span('Donor',icon("circle-info",id= "donor_info",style = "color: #dd4b39; position: absolute; right: 10px")), width = NULL, solidHeader = FALSE, status = "danger",
                                numericInput("ageD", "Donor age (years)", value = 60, min = 0, max = 100),
                                htmlOutput("note"),
                                radioButtons("causeDCD2cl", "Vascular cause of death", choiceNames=c('No','Yes'),choiceValues = c(0,1)),
                                radioButtons("diabD", "History of diabetes", choiceNames=c('No','Yes'),choiceValues = c(0,1)),
                                radioButtons("cmvD", "CMV serology", choiceNames=c('Negative','Positive'),choiceValues = c(0,1)),
                                radioButtons("htaD", "History of hypertension", choiceNames=c('No','Yes'),choiceValues = c(0,1))
                              ),
                              bsPopover(id="donor_info", title=NULL, content="Input donor information necessary for computing the marginality scores", trigger="hover", placement="left", options=list(container="body")),
                              box(
                                title = span('Recipient',icon("circle-info",id= "rec_info",style = "color: #3c8dbc; position: absolute; right: 10px")), width = NULL, solidHeader = FALSE, status = "primary",
                                numericInput("ageR", "Recipient Age (years)", value = 60, min = 18, max = 100),
                                radioButtons("incomp2cl", "ABDR incompatibilities", choiceNames=c('4 or less','5 or more'),choiceValues = c(0,1)),
                                htmlOutput("note3"),
                              ),
                              bsPopover(id="rec_info", title=NULL, content="Input the considered recipient&#39;s age and number of incompatibilities with the donor", trigger="hover", placement="left", options=list(container="body"))
                              
                              
                              
                       ),
                       
                       conditionalPanel(condition = 'input.ageR >= 18',
                                        column(width=7,
                                               box(title = span('FKDMS (absolute marginality)',icon("circle-info",id= "fkdms_info",style = "position: absolute; right: 10px"))
                                                   ,width=NULL,solidHeader = TRUE,status="info",
                                                   "Within all transplanted grafts",uiOutput('ecc',inline=T),"% have a better absolute marginality than the considered graft, with a FKDMS value of:",
                                                   div(style="text-align:center;
                                 font-size: 42pt;
                                 color: #4e4c4c",
                                                       textOutput("fkdms")))),
                                        bsPopover(id="fkdms_info", title=NULL, content="Graft marginality score. The higher the value, the higher the lower the expected patient and graft survival. Equals the relative risk for the considered graft compared to a graft from a 50 year-old donor without additional risk factors.", trigger="hover", placement="left", options=list(container="body")),
                                        column(width=7,
                                               box(title = span('RKDMS (recipient-wise marginality)',icon("circle-info",id= "rkdms_info",style = "position: absolute; right: 10px"))
                                                   ,width=NULL,solidHeader = TRUE,status="info",
                                                   'For a',uiOutput('ageR',inline=T),'year-old recipient with',conditionalPanel2(condition = 'input.incomp2cl == 1','5',inline = T),conditionalPanel2(condition = 'input.incomp2cl == 0','4',inline = T), 'ABDR incompatibilities',conditionalPanel2(condition = 'input.incomp2cl == 0','or less',inline = T),conditionalPanel2(condition = 'input.incomp2cl == 1','or more',inline = T) ,'with the donor, the graft has a RKDMS of:',
                                                   div(style="text-align:center;
                                 font-size: 42pt;
                                 color: #4e4c4c",
                                                       textOutput("rkdms")))),

                                        bsPopover(id="rkdms_info", title=NULL, content="Graft marginality score. The higher the value, the higher the lower the expected patient and graft survival for this given recipient", trigger="hover", placement="left", options=list(container="body")),
                                        column(width=7,
                                               box(title = span("Distribution of RKDMS values for grafts received by recipients from the same age group",icon("circle-info",id= "rkdmshist_info",style = "position: absolute; right: 10px")),
                                                   status = 'info',width = NULL,solidHeader=T,
                                                   "Among recipients between",
                                                   uiOutput('minq',inline = T), "and",
                                                   uiOutput('maxq',inline = T), "years old,",
                                                   uiOutput('percent',inline=T), "% have received a graft that was less prone to graft failure, i.e. with a lower RKDMS value.", 
                                                   plotOutput('histo')
                                               )),
                                        bsPopover(id="rkdmshist_info", title=NULL, content="The RKDMS of each graft received by a patient in the recipient&#39;s age group was computed as if this graft was transplanted to a patient with the considered recipient&#39;s age. The red dashed line indicates the considered graft&#39;s position in the distribution obtained this way. The color gradient indicates RKDMS quintiles: it the graft lies within the dark green segment, its RKDMS value lies within the top 20% best values. If it lies in the red segment, its RKDMS value lies within the 20% grafts with the worst values.", trigger="hover", placement="left", options=list(container="body")),
                                        column(width=5),
                                        column(width=7,
                                               
                                               box(title = span("Patient and graft survival by RKDMS quintile for recipients in the same age group",icon("circle-info",id= "rkdmscurve_info",style = "position: absolute; right: 10px")),
                                                   status = 'info',width = NULL,solidHeader=T,
                                                   plotOutput('survplot')
                                               ),
                                               bsPopover(id="rkdmscurve_info", title=NULL, content="Recipients from the recipient&#39;s age group were split into 5 classes according to RKDMS quintiles (corresponding to the above color gradient). Curves were estimated using the nonparametric Kaplan-Meier estimator.", trigger="hover", placement="left", options=list(container="body")),
                                               
                                        )))
               )
      
      
      
      
      ,tags$head(tags$style(HTML('
                              .content-wrapper { overflow: auto; }
                              
                              #options .box-header{ display: none}
                              
                              button.btn.dropdown-toggle.btn-default {
                                background-color: #d32927;
                                border-color: #d32927;
                                border-radius: 0px;
                              }

                              .box.box-danger{                                 
                                background:#f5eceb
                              }
                              
                              .box.box-warning{                                 
                                background:#f7f4ed
                              }
                              
                              .box.box-danger .form-control{                                 
                                background-color:#f5eceb !important
                              }
                              
                              .box.box-success{                                 
                                background:#e1fff1
                              }
                              
                              .box.box-success .form-control{                                 
                                background-color:#e1fff1 !important
                              }
                              
                              .box.box-primary{                                 
                                background:#ebf4f9
                              }
                              
                              .box.box-solid{                                 
                                background:#ffffff
                              }
                              
                              .navbar-nav > .messages-menu > .dropdown-menu > li .menu, .navbar-nav > .notifications-menu > .dropdown-menu > li .menu, .navbar-nav > .tasks-menu > .dropdown-menu > li .menu {
                                max-height: 500px; overflow-y: hidden;
                              }
                              
                              .box.box-primary .form-control{                                 
                                background-color:#ebf4f9 !important
                              }
                              
                              .box.box-solid.box-info{
                                background:#ffffff !important;
                                border-bottom-color:#4e4c4c;
                                border-left-color:#4e4c4c;
                                border-right-color:#4e4c4c;
                                border-top-color:#4e4c4c;
                              }
                              
                              .box.box-solid.box-info .box-header{
                                background:#4e4c4c !important
                              }
                              
                              .content {
                                height: auto; overflow-y: auto;
                              }
                              
                               .box.box-primary .radio{                                 
                                background-color:#ebf4f9 !important
                               }
                              
                              .box.box-danger .radio{                                 
                                background-color:#f5eceb !important
                              }
                    
                              /* navbar (rest of the header) */
                               .skin-blue .main-header .navbar {
                              background-color: #d32927;
                              }
                                            
                              /* navbar (rest of the header) */
                               .skin-blue .main-header .logo {
                              background-color: #d32927;
                              }              
                                            
                              /* logo when hovered */
                              .skin-blue .main-header .logo:hover {
                              background-color: #931c1b;
                              }              
                              /* toggle button when hovered  */                    
                              .skin-blue .main-header .navbar .sidebar-toggle:hover{
                              background-color: #931c1b;
                              }
                              
                              /* main sidebar */
                              .skin-blue .main-sidebar {
                              background-color: #4e4c4c;
                              }
                                            
                              /* active selected tab in the sidebarmenu */
                              .skin-blue .main-sidebar .sidebar .sidebar-menu .active a{
                              background-color: #282828;
                              border-left: 3px solid #d32927;
                              }
                              
                              
                              /* other links in the sidebarmenu when hovered */
                              .skin-blue .main-sidebar .sidebar .sidebar-menu a:hover{
                              background-color: #282828;
                              border-left: 3px solid #d32927;
                              }
              '))))
  )

# Define server logic required to draw a histogram
server <- function(input, output) { #,session
  library(dplyr)
  library(survival)
  library(pec)
  load('datashiny.RData')
  df_hist <- na.omit(df_hist)
  
  textnote<-reactive({
    if(input$ageD< 0){
      paste0("<font color=\"#e05959\"><b>", "Veuillez saisir un âge entre 0 et 90 ans", "</b></font>")
    }else{
      if (input$ageD > 90) {
        paste0("<font color=\"#e05959\"><b>", "Veuillez saisir un âge entre 0 et 90 ans", "</b></font>")
      } else {
        if (abs(input$ageR-input$ageD)>20) {
          paste0("<font color=\"#e05959\"><b>", "Avertissement: Âge du donneur et du receveur éloignés de plus de 20 ans", "</b></font>")
          
        }
      }
    }
  })
  
  textnote3<-reactive({
    if(input$ageR< 18){
      paste0("<font color=\"#e05959\"><b>", "Veuillez saisir un âge entre 18 et 90 ans", "</b></font>")
    }else{
      if (input$ageR > 90) {
        paste0("<font color=\"#e05959\"><b>", "Veuillez saisir un âge entre 18 et 90 ans", "</b></font>")
      } else {
        if (abs(input$ageR-input$ageD)>20) {
          paste0("<font color=\"#e05959\"><b>", "Avertissement: Âge du donneur et du receveur éloignés de plus de 20 ans", "</b></font>")
          
        }
      }
    }
  })
  
  output$note<-renderText({
    HTML(textnote() )
  })  

  output$note3<-renderText({
    HTML(textnote3() )
  })  
  
  
  dataReactive <- reactive({
    data.frame(ageD=input$ageD,causeDCD2cl=factor(input$causeDCD2cl,levels = c(0,1)),
                                       diabD=factor(input$diabD,levels=c(0,1)),
                                       cmvD=factor(input$cmvD,levels = c(0,1)),
                                       htaD=factor(input$htaD,levels = c(0,1)),
                                       incomp2cl=factor(input$incomp2cl,levels = c(0,1)),
                                       ageR=input$ageR,
                                       Tdial=3,
                                       anteDiab=factor(0,levels = c(0,1)),
                                       anteCardioVasc=factor(0,levels = c(0,1)),
                                       hemodial=factor(TRUE,levels = c(FALSE,TRUE)),
                                       TpsEvtYear=NA,Evt=NA)})

  rkdms <- reactive({
    drrr <- dataReactive()
    drrr$ageR <- drrr$ageR-50+1
    drrr$ageD <- drrr$ageD-50+1
    drrr$score <-(data.matrix(drrr)-1)[,1:5]%*%coef(mod_final)[1:5]
    drrr$ageR <- drrr$ageR+49
    drrr$ageD <- drrr$ageD+49
    drrr$rkdms <- 
      drrr$score +
      coef(mod_final2)[1]*(drrr$incomp2cl==1) +
      coef(mod_final2)[7]*(drrr$score) +
      coef(mod_final2)[8]*(drrr$score)*(drrr$ageR) 
    drrr$rkdms <- exp(drrr$rkdms)
    round(drrr$rkdms,2)
  })

  fkdms <- reactive({
    drrr <- dataReactive()
    drrr$ageR <- drrr$ageR-50+1
    drrr$ageD <- drrr$ageD-50+1
    round(exp((data.matrix(drrr)-1)[,1:5]%*%coef(mod_final)[1:5]),2)
  })
  
  ecc <- reactive({
    100*round(ecdf(exp(df_hist$score))(fkdms()),3)
  })
  
  dataReactive2 <- reactive({
    drrr <- dataReactive()
    drrr$ageR <- drrr$ageR-50+1
    drrr$ageD <- drrr$ageD-50+1
    drrr$score <-(data.matrix(drrr)-1)[,1:5]%*%coef(mod_final)[1:5]
    drrr$ageR <- drrr$ageR+49
    drrr$ageD <- drrr$ageD+49
    rkdms <- rkdms()
    cbind(drrr,rkdms)
  })
  
  df_hist00 <- reactive({
    df_dummy <- data.frame(ageR=rep(dataReactive2()[1,c('ageR')],nrow(df_hist)),
                           incomp2cl=df_hist$incomp2cl,
                           score=df_hist$score
    )
    rkdms <-   df_dummy$score +
      coef(mod_final2)[1]*(df_dummy$incomp2cl==1) +
      coef(mod_final2)[7]*(df_dummy$score) +
      coef(mod_final2)[8]*(df_dummy$score)*(df_dummy$ageR) 
    rkdms <- exp(rkdms)
    cbind(df_hist[,c('ageR','TpsEvtYear','Evt')],rkdms)
  })
  
  df_hist0 <- reactive({
    rbind(df_hist00(),dataReactive2()[,c('ageR','rkdms','TpsEvtYear','Evt')])})
  
  age_quantile <- reactive({
    cut(df_hist0()$ageR,quantile(df_hist0()$ageR,probs=seq(0,1,0.2)),labels=1:5,include.lowest = T)
  })
  df_hist2 <- reactive({
    cbind(df_hist0(),age_quantile())
  })
  
  df_histq <- reactive({
    df_hist2()[df_hist2()$age_quantile==df_hist2()[nrow(df_hist2()),]$age_quantile,]
  })
  percent <- reactive({
    round(ecdf(df_histq()$rkdms)(rkdms()),3)*100
  })
  histo <- reactive({
    par(mar=c(5.5,4.1,4.1,2.1))
    hist(df_histq()$rkdms,main=NULL, xlab='',ylab='Proportion (%)',probability=T,breaks=100,col='#706e6e')
    title(xlab='RKDMS',line=1.5)
    axis(1,at=rkdms(),labels=rkdms(),col='#d32927',col.axis='#d32927',lwd=2)
    axis(1,,at=c(quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[1],quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[2]),col="#0e6e18",line=3.6,lwd=10,lwd.ticks=0,labels=F)
    axis(1,,at=c(quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[2],quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[3]),col="#a3c716",line=3.6,lwd=10,lwd.ticks=0,labels=F)
    axis(1,,at=c(quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[3],quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[4]),col="#fad502",line=3.6,lwd=10,lwd.ticks=0,labels=F)
    axis(1,,at=c(quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[4],quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[5]),col="#fa8f02",line=3.6,lwd=10,lwd.ticks=0,labels=F)
    axis(1,,at=c(quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[5],quantile(df_histq()$rkdms,probs=seq(0,1,0.2))[6]),col="#c71616",line=3.6,lwd=10,lwd.ticks=0,labels=F)
    title(xlab='RKDMS quintiles',line=4.3)
    abline(v=rkdms(),col='#d32927',lty=2,lwd=2)
    usr <- par("usr")   # save old user/default/system coordinates
    par(usr = c(0, 1, 0, 1)) # new relative user coordinates
    legend("topright",legend=c(paste("Median =",round(median(df_histq()$rkdms,na.rm=T),2)),
                               paste("1st quintile =",round(quantile(df_histq()$rkdms,probs=seq(0,1,0.2),na.rm=T)[2],2)),
                               paste("2nd quintile =",round(quantile(df_histq()$rkdms,probs=seq(0,1,0.2),na.rm=T)[3],2)),
                               paste("3rd quintile =",round(quantile(df_histq()$rkdms,probs=seq(0,1,0.2),na.rm=T)[4],2)),
                               paste("4th quintile =",round(quantile(df_histq()$rkdms,probs=seq(0,1,0.2),na.rm=T)[5],2))                            ),
    )
    par(usr = usr) # restore original user coordinates
  })
  df_histq2 <- reactive({
    rkdms_quantile <- cut(df_histq()$rkdms,quantile(df_histq()$rkdms,probs=seq(0,1,0.2)),include.lowest=T)
    cbind(df_histq(),rkdms_quantile)
  })
  df_histq3 <- reactive({
    df_histq2()[df_histq2()$rkdms_quantile==df_histq2()[nrow(df_histq2()),]$rkdms_quantile,]
  })
  surviplot <- reactive({
    par(mar=c(4.1,4.1,2.1,2.1))
    plot(axes=F,survfit(Surv(df_histq2()$TpsEvtYear,df_histq2()$Evt)~df_histq2()$rkdms_quantile),lwd=2,xmax=10,col=c("#0e6e18","#a3c716","#fad502",
                                                                                                                     "#fa8f02",'#c71616'),xlab='Posttransplantation time (years)',ylab='Patient and graft survival probability')
    axis(1)
    axis(2)
    grid(nx=NA,ny=NULL,lty=2,col='lightgray',lwd=1)
    par(mar=c(4,4,0.1,4))
  })
  
  output$ecc <- renderText({HTML(paste0('<b>',ecc(),'</b>'))})
  output$rkdms <- renderText({rkdms()})
  output$fkdms <- renderText({fkdms()})
  output$rkdms2 <- renderText({rkdms()})
  output$rkdms3 <- renderText({rkdms()})
  output$ageR <- renderText({HTML(paste0('<b>',dataReactive()$ageR,'</b>'))})
  output$ageR8 <- renderText({HTML(paste0('<b>',dataReactive()$ageR,'</b>'))})
  output$ageR9 <- renderText({HTML(paste0('<b>',dataReactive()$ageR,'</b>'))})
  output$minq <- renderText({HTML(paste0('<b>',min(df_histq()$ageR),'</b>'))})
  output$maxq <- renderText({HTML(paste0('<b>',max(df_histq()$ageR),'</b>'))})
  output$minq2 <- renderText({HTML(paste0('<b>',min(df_histq()$ageR),'</b>'))})
  output$maxq2 <- renderText({HTML(paste0('<b>',max(df_histq()$ageR),'</b>'))})
  output$minq3 <- renderText({HTML(paste0('<b>',min(df_histq()$ageR),'</b>'))})
  output$maxq3 <- renderText({HTML(paste0('<b>',max(df_histq()$ageR),'</b>'))})
  output$percent <- renderText({HTML(paste0('<b>',percent(),'</b>'))})
  output$histo <- renderPlot({histo()})
  output$percent2 <- renderText({HTML(paste0('<b>',percent(),'</b>'))})
  output$survplot <- renderPlot({surviplot()})
  #if (!interactive()) {
  #session$onSessionEnded(function() {
  #stopApp()
  #q("no")
  #})
  #}
  
}



# Run the application 
shinyApp(ui = ui, server = server)
